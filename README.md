**Deploiement du FAIR Data Point EOSC-PILLAR (logo) sur une instance Kubernetes**

Images Docker utilisées : 


* https://hub.docker.com/repository/docker/cines/cines-fdp
* https://hub.docker.com/repository/docker/cines/cines-fdp-client



./start permet de deployer les services sur kubernetes


./stop permet de supprimer les services sur kubernetes


Le pod fdp-client est de type LoadBalancer (adresse ip exterieure disponible)


Base de données utilisées: 

* Mongodb (fdp)
* Blazegraph (triple store - DCAT)
